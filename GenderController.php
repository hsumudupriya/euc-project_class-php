<?php

include_once 'GenderDao.php';

class GenderController {
	public static function get() {
		$genders = GenderDao::getAll();
		echo json_encode($genders);
	}
}
