<?php

include_once 'db.php';
include_once 'Student.php';
include_once 'GenderDao.php';

class StudentDao {
	public static function getAll() {
		global $dbcon;

		$query = 'SELECT * FROM student';

		return self::get($query);
	}

	public static function getAllByNic($nic) {
		$query = 'SELECT * FROM student WHERE nic LIKE :nic';
		$params = ['nic' => "$nic%"];

		return self::get($query, $params);
	}

	public static function getAllByName($name) {
		$query = 'SELECT * FROM student WHERE name LIKE :name';
		$params = ['name' => "$name%"];

		return self::get($query, $params);
	}

	public static function getAllByNicAndName($nic, $name) {
		$query = 'SELECT * FROM student WHERE nic LIKE :nic AND name LIKE :name';
		$params = ['nic' => "$nic%", 'name' => "$name%"];

		return self::get($query, $params);
	}

	public static function get($query, $params = []) {
		global $dbcon;

		$stmt = $dbcon->prepare($query);
		$stmt->execute($params);

		$students = $stmt->fetchAll(PDO::FETCH_CLASS, \Student::class);
		$genders = GenderDao::getAll();

		foreach($students as $student) {
			foreach($genders as $gender) {
				if ($student->getGenderId() == $gender->getId()) $student->setGender($gender);
			}
		}

		return $students;
	}

	public static function save($student) {
		global $dbcon;

		$data = ['name' => $student->name, 'nic' => $student->nic, 'gender_id' => $student->gender->id];
		$sql = 'INSERT INTO student (name, nic, gender_id) VALUES (:name, :nic, :gender_id)';
		$stmt = $dbcon->prepare($sql);

		return $stmt->execute($data);
	}

	public static function update($student) {
		global $dbcon;

		$data = ['nic' => $student->nic, 'gender_id' => $student->gender->id, 'name' => $student->name];
		$sql = 'UPDATE student SET nic = :nic, gender_id = :gender_id WHERE name = :name';
		$stmt = $dbcon->prepare($sql);

		return $stmt->execute($data);
	}

	public static function delete($student) {
		global $dbcon;

		$data = ['name' => $student->name];
		$sql = 'DELETE FROM student WHERE name = :name';
		$stmt = $dbcon->prepare($sql);

		return $stmt->execute($data);
	}
}
