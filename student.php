<?php

class Student {
	public $name;
	public $nic;
	public $gender_id;
	public $gender;

	function __construct() {}

	function setName($name) { $this->name = $name; }
	function setNic($nic) { $this->nic = $nic; }
	function setGenderId($gender_id) { $this->gender_id = $gender_id; }
	function setGender($gender) { $this->gender = $gender; }

	function getName() { return $this->name; }
	function getNic() { return $this->nic; }
	function getGenderId() { return $this->gender_id; }
	function getGender() { return $this->gender; }
}
