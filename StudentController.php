<?php

include_once 'StudentDao.php';

class StudentController {
	public static function get($params) {
		if (isset($params['nic']) && isset($params['name']))
			$students = StudentDao::getAllByNicAndName($params['nic'], $params['name']);
		elseif (isset($params['name']))
			$students = StudentDao::getAllByName($params['name']);
		elseif (isset($params['nic']))
			$students = StudentDao::getAllByNic($params['nic']);
		else
			$students = StudentDao::getAll();

		echo json_encode($students);
	}

	public static function post($student) {
		$student = json_decode($student);

		if (StudentDao::save($student) === true) echo '201';
		else echo '501';
	}

	public static function put($student) {
		$student = json_decode($student);

		if (StudentDao::update($student) === true) echo '201';
		else echo '501';
	}

	public static function delete($student) {
		$student = json_decode($student);

		if (StudentDao::delete($student) === true) echo '201';
		else echo '501';
	}
}
