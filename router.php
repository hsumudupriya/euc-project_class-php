<?php

$method = $_SERVER['REQUEST_METHOD'];
$urlandparams = explode('?', $_SERVER['REQUEST_URI']);
$params = isset($urlandparams[1]) ? $urlandparams[1] : '';

$uriparts = explode('/', $urlandparams[0]);
$resource = isset($uriparts[1]) ? $uriparts[1] : null;
if ($uriparts >= 3) $ui = isset($uriparts[2]) ? $uriparts[2] : null;

switch ($resource) {
	case 'genders':
		include 'GenderController.php';

		switch ($method) {
			case 'GET':
				GenderController::get();
			break;

			case 'POST':
				echo 'Under construction';
			break;

			case 'PUT':
				echo 'Under construction';
			break;

			case 'DELETE':
				echo 'Under construction';
			break;
			
			default:
				echo 'Unknown method';
			break;
		}
	break;

	case 'students':
		include 'StudentController.php';

		switch ($method) {
			case 'GET':
				StudentController::get($_GET);
			break;

			case 'POST':
				StudentController::post($_POST['student']);
			break;

			case 'PUT':
				$_PUT = [];
				parse_str(file_get_contents('php://input'), $_PUT);
				StudentController::put($_PUT['student']);
			break;

			case 'DELETE':
				$_DELETE = [];
				parse_str(file_get_contents('php://input'), $_DELETE);
				StudentController::delete($_DELETE['student']);
			break;
			
			default:
				echo 'Unknown method';
			break;
		}
	break;

	case 'ui':
		switch ($ui) {
			case 'students':
				include_once 'studentui.html';
			break;

			default:
				echo 'Invalid UI';
			break;
		}
	break;

	default:
		echo 'Incorrect URL';
	break;
}
