<?php
$host = '127.0.0.1:3306';
$user = 'root';
$pass = '';
$db = 'sumududb';

$con = mysqli_connect($host, $user, $pass, $db);

if (isset($_POST['formadd']) && $_POST['formadd'] == '1') {
    $query = 'INSERT INTO student VALUES ("'.$_POST['name'].'", "'.$_POST['nic'].'", "'.$_POST['gender'].'")';
    $result = mysqli_query($con, $query);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Student Management</h1>
    <table border="0">
        <tr>
            <td rowspan="2" style="vertical-align: top;">
                <h2 style="margin-top: 0;">Student Registration Form</h2>
	            <form action="studentui.php" method="post">
                    <div style="margin-bottom: 10px;">
                        <label style="margin-right: 10px;" for="name">Name</label>
                        <input type="text" name="name" id="name" tabindex="1" placeholder="Type your name">
                    </div>
                    <div style="margin-bottom: 10px;">
                        <label style="margin-right: 10px;" for="nic">NIC</label>
                        <input type="text" name="nic" id="nic" placeholder="Type your NIC">
                    </div>
                    <div style="margin-bottom: 10px;">
                        <label style="margin-right: 10px;" for="gender">Gender</label>
                        <select name="gender" id="gender">
                            <?php
                            $query = 'SELECT id, name FROM gender';
                            $genders = mysqli_query($con, $query);

                            foreach($genders as $gender):
                            ?>
                            <option value="<?php echo $gender['id']; ?>"><?php echo $gender['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div>
                        <input type="hidden" name="formadd" value="1">
                        <input type="reset" value="Reset">
                        <input type="submit" value="Add">
                    </div>
                </form>
            </td>
            <td style="padding: 10px;">
                <form action="studentui.php" method="get">
                    <div style="margin-bottom: 10px;">
                        <label style="margin-right: 10px;" for="q_nic">Search by NIC:</label>
                        <input id="q_nic" name="q_nic" type="text" value="<?php if (isset($_GET['q_nic'])) echo $_GET['q_nic']; ?>">
                    </div>
                    <input type="submit" value="Search">
                </form>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px;">
                <table border="1">
                    <thead>
                        <th>Name</th>
                        <th>NIC</th>
                        <th>Gender</th>
                    </thead>
                    <tbody>
                    <?php
                    $query = 'SELECT * FROM student';
                    if (isset($_GET['q_nic'])) $query .= ' WHERE nic LIKE \'%'.$_GET['q_nic'].'%\'';
                    $query .= ' ORDER BY name';
                    $students = mysqli_query($con, $query);

                    foreach($students as $student):
                    ?>
                    <tr style="<?php if (isset($_POST['formadd']) && $_POST['nic'] == $student['nic']) echo 'background-color: lightgreen;' ?>">
                        <td><?php echo $student['name']; ?></td>
                        <td><?php echo $student['nic']; ?></td>
                        <td><?php echo $student['gender_id']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>