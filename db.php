<?php
$dbhost = '127.0.0.1:3306';
$dbuser = 'homestead';
$dbpass = 'secret';
$dbname = 'euc_project_class';
$charset = 'utf8mb4';

$dbstring = 'mysql:host='.$dbhost.';dbname='.$dbname.';charset='.$charset;
$dbcon = new \PDO($dbstring, $dbuser, $dbpass);
