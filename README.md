PHP tutorial of project class of Earth University College
===

This is the complete PHP tutorial of the project class of Earth University College.

[TOC]

### Day 01 (2019-12-07)

##### index.php

	index.php is the default file that is run by the Apache when you access the root folder of the server.

##### home.php

	Let's instruct Apache to run home.php by default instead of index.php, when the root folder of the server is accessed.

##### .htaccess

	We write local instructions for the Apache server in a file named .htaccess. 
	In that file you can intrust the Apache server about how to behave within the folder.

##### areacal.php

	Access two parameters from the GET method, multiply them to calculate area and print the output.

##### db/sumududb.mwb

	MySQL Workbench file which contains the EER diagram of the database. 
	Database contains a student table with three columns. name [varchar(100)], nic [varchar(10)] and gender [char(8)].

##### studentview.php

	Fetch and display student records in the student database table in a HTML table.

##### studentsearch.php

	Search for students in the database table using their NICs. 
	Get the NIC that needs to be searched as a GET parameter, fetch the filtered result from the database and display the student records as a HTML table.

##### studentform.html

	Create a HTMl form to add new students.

##### studentregister.php

	Form in the studentform.html file is submitted to the studentregister.php. 
	In that file, a new user is created in the database table using the Name, NIC and Gender submitted by the HTML form. 
	At the end of the studentregister.php file, studentform.html and studentview.php files are included to display form again and the student records.

##### studentui.php

	Integrate studentview.php, studentsearch.php and studentform.html file into a single file, so that user can add, view and search for students in one view. 
	This improves the user friendlyness of the website.

##### db/sumududb_******.sql

	Backup of the database as an .sql file at the end of the day.


### Day 02 (2019-12-14)

##### studentui.php

	Now that we store genders in the gender table in the database, genders dropdown is populated with the genders fetched from the database table.

##### studentui.html

	In order to shift from traditional form submition to AJAX based form submition, we have removed all the PHP codes from the studentui.php file and changed its' extension to HTML. 
	This improves user friendlyness of our web page. 
	XMLHttpRequests are implemented to make AJAX calls to the web server.

##### student.php

	Student records are fetched from the database and echoed as HTML table rows.

##### gender.php

	Genders are fetched from the databse and echoed as HTML select options.


### Day 03 (2019-12-21)

##### studentui.html

	Gender dropdown is populated via AJAX. Submitting student form via AJAX to add a new student is implemented.

##### student.php

	Student view/search and register actions, which were in separate files are integrated into one file. 
	Actions are initiated based on the method of the HTTP request. 
	Student view and search action uses HTTP GET method and student register action use HTTP POST method. 
	This is the initial step towards REST architechural style.

### Day 04 (2020-01-04)

	Implemented the missing feature of highlighting the newly added student record, first using session. 
	But using session in a RESTful service is not approved as REST is stateless and session is stateful. 
	So, cookies were used instead of session. 

	Returning output as HTML is platform dependant. A RESTful service should be platform independant. 
	So, JSON is used as the output format as it is cross-platform supported. 

	Implemeted a router to hide the implementation/technology used in the web server from the users.

	Started to shift into MVC architecture by implementing model/entity classes, DAO classes and controller classes.

### Day 05 (2020-01-11)

	A new column "color" is added to the database table gender. 
	Gender entity class, DAO class and the controller class are also changed to include the new tabel column.

	Properties of the Gender and Student entity classes are made public. 
	So, array objects can be directly converted into a JSON string using json_encode() function without having to manually manipulate them first.

	Database connection is changed to use PHP Data Objects(PDO) library instead of mysqli. 
	So, we don't have to create objects from entity classes and set the data fetched from the database table to them manually. 
	Instead we can pass the class name into PDO and PDO will create entity objects for us.
	Had to change user defined contructors in the entity classes into default contrustors. 
	Because, PDO can't use user defined constructors.

	Eager loading is implemented in student objects. 
	So, gender object of each student can be accessed from the student object.

	router.php is updated to address HTTP methods (GET, POST, PUT, DELETE) and to provide access to HTML UIs.

##### studentui.html

	URLs of XMLHttpRequests are updated to suit the new URL format. 
	(i.e. PHP extension is removed from the URLS)

	Genders in the Gender dropdown is populated using the JSON string.

	JS prompt shown when submitting the student form, now shows name of the selected gender instead of the gender id.

	Gender dropdown is updated to show color for each gender. 
	JS prompt is updated to show color of the selected gender.

	Student rows in the students table is populated using the JSON string.
