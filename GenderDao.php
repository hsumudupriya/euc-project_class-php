<?php

include_once 'db.php';
include_once 'Gender.php';

class GenderDao {
	static function getAll() {
		global $dbcon;

		$query = 'SELECT * FROM gender';
		$stmt = $dbcon->prepare($query);
		$stmt->execute();

		$genders = $stmt->fetchAll(PDO::FETCH_CLASS, \Gender::class);

		return $genders;
	}
}
