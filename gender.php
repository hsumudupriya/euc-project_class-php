<?php

class Gender {
	public $id;
	public $name;
	public $color;

	function __construct() {}

	function setId($id) { $this->id = $id; }
	function setName($name) { $this->name = $name; }
	function setColor($color) { $this->color = $color; }

	function getId() { return $this->id; }
	function getName() { return $this->name; }
	function getColor() { return $this->color; }
}
